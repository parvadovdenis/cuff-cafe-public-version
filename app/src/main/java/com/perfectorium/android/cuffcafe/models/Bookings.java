package com.perfectorium.android.cuffcafe.models;

/**
 * Created by Denys Parvadov on 03.11.16.
 */

public class Bookings {

    private String email;
    private String name;
    private int people;
    private String phone;
    private int status;
    private long time_booked;
    private long time_made;
    private String token;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    public String getToken() {
        return this.token;
    }

    public void setToken(String id) {
        this.token = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPeople() {
        return people;
    }

    public void setPeople(int people) {
        this.people = people;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getTime_booked() {
        return time_booked;
    }

    public void setTime_booked(long time_booked) {
        this.time_booked = time_booked;
    }

    public long getTime_made() {
        return time_made;
    }

    public void setTime_made(long time_made) {
        this.time_made = time_made;
    }

    public Bookings(String email, String name, int people,
                    String phone, int status,
                    long time_booked, long time_made) {
        this.email = email;
        this.name = name;
        this.people = people;
        this.phone = phone;
        this.status = status;

        this.time_booked = time_booked;
        this.time_made = time_made;
    }

    public Bookings() {

    }

}
