package com.perfectorium.android.cuffcafe.models;

/**
 * Created by Denys Parvadov on 19.10.16.
 */

public class News {

    String image;
    String title;
    String link;
    String id;

    public String getImage_storage_id() {
        return image_storage_id;
    }

    public void setImage_storage_id(String image_storage_id) {
        this.image_storage_id = image_storage_id;
    }

    String image_storage_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public News(String title, String image) {

        this.title = title;
        this.image = image;
    }

    public News() {

    }
}
