package com.perfectorium.android.cuffcafe;

import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Patterns;
import android.view.Display;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.perfectorium.android.cuffcafe.adapters.NewsAdapter;
import com.perfectorium.android.cuffcafe.config.FireBaseConfig;
import com.perfectorium.android.cuffcafe.models.News;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends BaseActivity implements NewsAdapter.OnClickListener {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<News> n = new ArrayList<>();
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_home;
    }

    @Override
    protected Class getCurrentClass() {
        return MainActivity.class;
    }

    @Override
    protected void setRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.news_list);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new NewsAdapter(n, this, this);
        //set Header
        RecyclerViewHeader header = (RecyclerViewHeader) findViewById(R.id.header);
        header.attachTo(recyclerView);
        recyclerView.setAdapter(adapter);
        ImageView home_header = (ImageView) findViewById(R.id.home_header);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        home_header.getLayoutParams().width = width;
        home_header.getLayoutParams().height = height / 5;
        home_header.requestLayout();
    }


    @Override
    protected void setFireBase() {
        Firebase.setAndroidContext(this);
        Firebase ref = new Firebase(FireBaseConfig.FIREBASE_URL);
        DatabaseReference scoresRef = FirebaseDatabase.getInstance().getReference("news");
        scoresRef.keepSynced(true);
        ref.addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                progressBar = (ProgressBar) findViewById(R.id.prb);
                progressBar.setVisibility(ProgressBar.VISIBLE);
                n.clear();
                for (DataSnapshot postSnapShot : snapshot.child("news").getChildren()) {
                    News news = postSnapShot.getValue(News.class);
                    n.add(news);
                }
                Collections.reverse(n);
                progressBar.setVisibility(ProgressBar.INVISIBLE);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }

    @Override
    public void onClick(News news) {
        String url = news.getLink();
        if (!url.equals("") && url != null
                && Patterns.WEB_URL.matcher(url).matches()) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        }
    }
}
