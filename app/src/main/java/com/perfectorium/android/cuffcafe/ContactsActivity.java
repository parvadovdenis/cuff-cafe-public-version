package com.perfectorium.android.cuffcafe;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.perfectorium.android.cuffcafe.adapters.SwipeAdapter;
import com.perfectorium.android.cuffcafe.config.TextFont;

import java.util.ArrayList;

/**
 * Created by Denys Parvadov on 16.10.16.
 */

public class ContactsActivity extends BaseActivity {
    private ViewPager viewPager;
    private SwipeAdapter swipeAdapter;
    private int[] content;

    private TextView text1;
    private TextView text2;
    private TextView text3;
    private TextView text4;
    private TextView text5;
    private TextView text6;
    private TextView text7;


    @Override
    protected int getContentView() {
        return R.layout.activity_contacts;
    }

    @Override
    protected Class getCurrentClass() {
        return ContactsActivity.class;
    }

    private int[] setListContent() {
        content = new int[]{
                R.drawable.slider_contacts_1,
                R.drawable.slider_contacts_2,
                R.drawable.slider_contacts_3,
                R.drawable.slider_contacts_4
        };
        return content;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        text1 = (TextView) findViewById(R.id.contacts_textView);
        text2 = (TextView) findViewById(R.id.contacts_list_title);
        text3 = (TextView) findViewById(R.id.contacts_link_location);
        text4 = (TextView) findViewById(R.id.contacts_link_location);
        text5 = (TextView) findViewById(R.id.contacts_phone);
        text6 = (TextView) findViewById(R.id.contacts_email);
        text7 = (TextView) findViewById(R.id.contacts_time);

        TextFont.setJosefinsSansRegular(text1, this);
        TextFont.setJosefinsSansRegular(text2, this);
        TextFont.setJosefinsSansRegular(text3, this);
        TextFont.setJosefinsSansRegular(text4, this);
        TextFont.setJosefinsSansRegular(text5, this);
        TextFont.setJosefinsSansRegular(text6, this);
        TextFont.setJosefinsSansRegular(text7, this);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void setViewPager() {

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        swipeAdapter = new SwipeAdapter(this, null, true);
        swipeAdapter.setList(setListContent());
        viewPager.setAdapter(swipeAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                indicatorDots(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        clickLink();

        ImageView fab = (ImageView) findViewById(R.id.call_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse(getString(R.string.phone_number)));
                startActivity(callIntent);
            }
        });

    }

    private void indicatorDots(int position) {
        LinearLayout l = (LinearLayout) findViewById(R.id.indicator);

        ImageView indicator_1 = (ImageView) l.findViewById(R.id.indicator_1);
        ImageView indicator_2 = (ImageView) l.findViewById(R.id.indicator_2);
        ImageView indicator_3 = (ImageView) l.findViewById(R.id.indicator_3);
        ImageView indicator_4 = (ImageView) l.findViewById(R.id.indicator_4);

        ArrayList<ImageView> dots = new ArrayList<>();

        dots.add(indicator_1);
        dots.add(indicator_2);
        dots.add(indicator_3);
        dots.add(indicator_4);

        int selected = 20;
        int unselected = 15;

        for (ImageView dot : dots) {
            dot.getLayoutParams().height = unselected;
            dots.get(position).getLayoutParams().height = selected;
        }

        l.requestLayout();
    }

    private void clickLink() {
        TextView link = (TextView) findViewById(R.id.contacts_link_location);
        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = getString(R.string.url_link_location);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        });
    }
}

