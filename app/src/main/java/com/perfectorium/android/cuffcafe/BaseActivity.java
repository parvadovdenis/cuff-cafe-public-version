package com.perfectorium.android.cuffcafe;

/**
 * Created by Denys Parvadov on 15.10.16.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.perfectorium.android.cuffcafe.config.FireBaseConfig;
import com.perfectorium.android.cuffcafe.config.PrefManager;
import com.perfectorium.android.cuffcafe.models.Bookings;


public abstract class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private final int HOME_ACTIVITY = R.layout.activity_home;
    private final int MENU_ACTIVITY = R.layout.activity_menu;
    private final int BOOKING_ACTIVITY = R.layout.activity_booking;
    private final int CONTACTS_ACTIVITY = R.layout.activity_contacts;

    private Class current_class;


    private final int BOOKING_YES = R.layout.activity_booking_yes;
    private final int BOOKING_NO = R.layout.activity_booking_no;
    private final int BOOKING_WAIT = R.layout.activity_booking_wait;
    private final int BOOKING_YES_CHECK = R.layout.activity_booking_yes_check;

    private ProgressDialog progressDialog;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigationView;

    private DatabaseReference mDatabse;

    private ActionBar actionBar;


    public Toolbar getToolbar() {
        return toolbar;
    }

    protected DatabaseReference getDatabaseRefence() {
        this.mDatabse = FirebaseDatabase.getInstance().getReference();
        return this.mDatabse;
    }

    protected DatabaseReference getDatabase() {
        return this.mDatabse;
    }

    protected void bookingViews() {

    }

    private Class bookingActivityClass = BookingActivity.class;

    private void onNavigationItemSelected(final Class cl) {
        PrefManager prefManager = new PrefManager(this);
        final String booking_id = prefManager.getBoookingId();

        Firebase.setAndroidContext(this);
        Firebase ref = new Firebase(FireBaseConfig.FIREBASE_URL);
        ref.addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot postSnapShot : snapshot.child("bookings").getChildren()) {
                    Bookings booking = postSnapShot.getValue(Bookings.class);
                    String getKey = postSnapShot.getKey();
                    if (getKey.equals(booking_id))
                        switch (booking.getStatus()) {
                            case 0:
                                bookingActivityClass = BookingWaitActivity.class;
                                break;
                            case 1:
                                bookingActivityClass = BookingYesActivity.class;
                                break;
                            case 2:
                                bookingActivityClass = BookingNoActivity.class;
                                break;
                        }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });

        RelativeLayout nav_home = (RelativeLayout) findViewById(R.id.nav_home);
        RelativeLayout nav_menu = (RelativeLayout) findViewById(R.id.nav_menu);
        RelativeLayout nav_booking = (RelativeLayout) findViewById(R.id.nav_booking);
        RelativeLayout nav_contacts = (RelativeLayout) findViewById(R.id.nav_contacts);

        nav_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cl != MainActivity.class)
                    startActivity(MainActivity.class);
                else
                    drawer.closeDrawer(GravityCompat.END);
            }
        });
        nav_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cl != MenuActivity.class)
                    startActivity(MenuActivity.class);
                else
                    drawer.closeDrawer(GravityCompat.END);
            }
        });
        nav_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cl != BookingActivity.class)
                    startActivity(bookingActivityClass);
                else
                    drawer.closeDrawer(GravityCompat.END);
            }
        });
        nav_contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cl != ContactsActivity.class)
                    startActivity(ContactsActivity.class);
                else
                    drawer.closeDrawer(GravityCompat.END);
            }
        });
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }

    protected void setViewPager() {

    }

    protected void setRecyclerView() {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FirebaseApp.initializeApp(this);
        if (!Firebase.getDefaultConfig().isPersistenceEnabled())
            Firebase.getDefaultConfig().setPersistenceEnabled(true);
        super.onCreate(savedInstanceState);
        int contentViewId = getContentView();
        if (contentViewId > 0) {
            setContentView(contentViewId);
            if (contentViewId == R.layout.activity_contacts) {
                setViewPager();
            }
        }
    }

    protected void styleActionBar(ActionBar actionBar) {
    }

    @Nullable
    private Bundle getExtras(Bundle savedInstanceState) {
        Bundle extras;
        if (savedInstanceState == null) {
            extras = getIntent().getExtras();
        } else {
            extras = savedInstanceState;
        }
        return extras;
    }

    protected abstract int getContentView();

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.open_right_drawer) {
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.openDrawer(GravityCompat.END);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected abstract Class getCurrentClass();


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        attachActivityViews();
        onNavigationItemSelected(getCurrentClass());
        initActivityViews();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            setupActionBar(actionBar);
            styleActionBar(actionBar);
        }
        setFireBase();
        setRecyclerView();
        setMenu();
        bookingViews();
    }

    protected void setFireBase() {

    }

    protected void setMenu() {

    }


    private void setToolbarBackground(int activity_layout, Toolbar toolbar) {
        int drawable;
        String title = "";
        if (activity_layout != HOME_ACTIVITY) {
            drawable = R.drawable.header_without_logo;
            switch (activity_layout) {
                case MENU_ACTIVITY:
                    title = "Menu";
                    break;
                case BOOKING_ACTIVITY:
                    title = "Booking";
                    break;
                case CONTACTS_ACTIVITY:
                    title = "Contacts";
                    break;
                case BOOKING_NO:
                    title = "Booking";
                    break;
                case BOOKING_YES:
                    title = "Booking";
                    break;
                case BOOKING_YES_CHECK:
                    title = "Booking";
                    break;
                case BOOKING_WAIT:
                    title = "Booking";
                    break;
            }
        } else
            drawable = R.drawable.header_home;

        TextView t = (TextView) findViewById(R.id.toolbar_title);
        t.setText(title);
        toolbar.setBackgroundResource(drawable);
    }

    protected void startActivity(Class activityClass) {
        Intent intent = new Intent(this, activityClass);
        startActivity(intent);
    }

    @StringRes
    protected int getActionBarTitle() {
        return R.string.empty_title;
    }

    protected String getActionBarTitleString() {
        return null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            String myTitle = getActionBarTitleString();
            if (myTitle != null) {
                actionBar.setTitle(myTitle);
            } else {
                actionBar.setTitle(getActionBarTitle());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the ic_menu_menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    protected void attachActivityViews() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setToolbarBackground(getContentView(), toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
    }

    protected void initActivityViews() {
        setSupportActionBar(toolbar);
        navigationView.setNavigationItemSelectedListener(this);
    }

    protected void setupActionBar(ActionBar actionBar) {
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View v = this.getCurrentFocus();
        if (v != null) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            v.clearFocus();
        }
    }

    protected void replace(int resId, Fragment fragment, boolean isAddToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(resId, fragment);
        if (isAddToBackStack)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void showWaitingDialog(String waitingMessage, boolean cancelable) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
        }
        progressDialog.setCancelable(cancelable);
        if (waitingMessage != null) {
            progressDialog.setMessage(waitingMessage);
        }
        if (progressDialog != null && !progressDialog.isShowing() && !isFinishing()) {
            progressDialog.show();
        }
    }

    public void showWaitingDialog(String waitingMessage) {
        showWaitingDialog(waitingMessage, false);
    }

    public void showWaitingDialog() {
        showWaitingDialog();
    }

    public void dismissWaitingDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        dismissWaitingDialog();
        super.onDestroy();
    }

    public void showInformationDialog(int message) {
        DialogFragment dialog = InformationDialog.newInstance(0, message);
        dialog.show(getSupportFragmentManager(), "info_dialog");
    }

    public void showInformationDialog(int title, int message) {
        DialogFragment dialog = InformationDialog.newInstance(title, message);
        dialog.show(getSupportFragmentManager(), "info_dialog");
    }

    public void showInformationDialog(String message) {
        DialogFragment dialog = InformationDialog.newInstance(null, message);
        dialog.show(getSupportFragmentManager(), "info_dialog");
    }

    public void showInformationDialog(String title, String message) {
        DialogFragment dialog = InformationDialog.newInstance(title, message);
        dialog.show(getSupportFragmentManager(), "info_dialog");
    }

    public void showInformationDialog(@StringRes int titleId, @StringRes int messageId,
                                      DialogInterface.OnClickListener listener) {
        DialogFragment dialog = InformationDialog.newInstance(titleId, messageId, listener);
        dialog.show(getSupportFragmentManager(), "info_dialog");
    }

    public void showInformationDialog(String title, String message, DialogInterface.OnClickListener listener) {
        DialogFragment dialog = InformationDialog.newInstance(title, message, listener);
        dialog.show(getSupportFragmentManager(), "info_dialog");
    }

    public void showInformationDialog(int title, int message, DialogInterface.OnClickListener listener,
                                      DialogInterface.OnCancelListener cancelListener) {
        DialogFragment dialog = InformationDialog.newInstance(title, message, listener, cancelListener);
        dialog.show(getSupportFragmentManager(), "info_dialog");
    }

    public void showInformationDialog(String title, String message, DialogInterface.OnClickListener listener,
                                      DialogInterface.OnCancelListener cancelListener) {
        DialogFragment dialog = InformationDialog.newInstance(title, message, listener, cancelListener);
        dialog.show(getSupportFragmentManager(), "info_dialog");
    }

    public void showInformationDialog(int titleId, int messageId, int positiveId, int negativeId,
                                      DialogInterface.OnClickListener positiveListener,
                                      DialogInterface.OnClickListener negativeListener) {
        DialogFragment dialog = InformationDialog.newInstance(titleId, messageId, positiveId,
                negativeId, positiveListener, negativeListener);
        dialog.show(getSupportFragmentManager(), "info_dialog");
    }

    public static class InformationDialog extends DialogFragment {
        private static final String TITLE_INT_BUNDLE_KEY = "title_int";
        private static final String MESSAGE_INT_BUNDLE_KEY = "message_int";
        private static final String TITLE_STRING_BUNDLE_KEY = "title_string";
        private static final String MESSAGE_STRING_BUNDLE_KEY = "message_string";
        private static final String POSITIVE_BTN_INT_BUNDLE_KEY = "positive_btn_int";
        private static final String NEGATIVE_BTN_INT_BUNDLE_KEY = "negative_btn_int";

        private DialogInterface.OnClickListener clickListener;
        private DialogInterface.OnCancelListener cancelListener;
        private DialogInterface.OnClickListener negativeClickListener;

        public static InformationDialog newInstance(int title, int message,
                                                    DialogInterface.OnClickListener listener) {
            Bundle args = new Bundle();
            args.putInt(TITLE_INT_BUNDLE_KEY, title);
            args.putInt(MESSAGE_INT_BUNDLE_KEY, message);
            return newDialog(args, listener, null, null);
        }

        public static InformationDialog newInstance(String title, String message,
                                                    DialogInterface.OnClickListener listener) {
            Bundle args = new Bundle();
            args.putString(TITLE_STRING_BUNDLE_KEY, title);
            args.putString(MESSAGE_STRING_BUNDLE_KEY, message);
            return newDialog(args, listener, null, null);
        }

        public static InformationDialog newInstance(int title, int message,
                                                    DialogInterface.OnClickListener listener,
                                                    DialogInterface.OnCancelListener cancelListener) {
            Bundle args = new Bundle();
            args.putInt(TITLE_INT_BUNDLE_KEY, title);
            args.putInt(MESSAGE_INT_BUNDLE_KEY, message);
            return newDialog(args, listener, cancelListener, null);
        }

        public static InformationDialog newInstance(String title, String message,
                                                    DialogInterface.OnClickListener listener,
                                                    DialogInterface.OnCancelListener cancelListener) {
            Bundle args = new Bundle();
            args.putString(TITLE_STRING_BUNDLE_KEY, title);
            args.putString(MESSAGE_STRING_BUNDLE_KEY, message);
            return newDialog(args, listener, cancelListener, null);
        }

        public static InformationDialog newInstance(int title, int message) {
            return newInstance(title, message, null);
        }

        public static InformationDialog newInstance(String title, String message) {
            return newInstance(title, message, null);
        }

        public static InformationDialog newInstance(int titleId, int messageId, int positiveId,
                                                    int negativeId, DialogInterface.OnClickListener positiveListener,
                                                    DialogInterface.OnClickListener negativeListener) {
            Bundle args = new Bundle();
            args.putInt(TITLE_INT_BUNDLE_KEY, titleId);
            args.putInt(MESSAGE_INT_BUNDLE_KEY, messageId);
            args.putInt(POSITIVE_BTN_INT_BUNDLE_KEY, positiveId);
            args.putInt(NEGATIVE_BTN_INT_BUNDLE_KEY, negativeId);
            return newDialog(args, positiveListener, null, negativeListener);
        }

        private static InformationDialog newDialog(Bundle args, DialogInterface.OnClickListener okListener,
                                                   DialogInterface.OnCancelListener cancelListener,
                                                   DialogInterface.OnClickListener negativeListener) {
            InformationDialog dialog = new InformationDialog();
            dialog.setPositiveOnClickListener(okListener);
            dialog.setNegativeOnClickListener(negativeListener);
            dialog.setOnCancelListener(cancelListener);
            dialog.setArguments(args);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            Bundle args = getArguments();
            if (args.containsKey(TITLE_INT_BUNDLE_KEY) && args.containsKey(MESSAGE_INT_BUNDLE_KEY)) {
                int title = args.getInt(TITLE_INT_BUNDLE_KEY);
                if (title != 0) builder.setTitle(title);
                int message = args.getInt(MESSAGE_INT_BUNDLE_KEY);
                builder.setMessage(message);
            } else if (args.containsKey(TITLE_STRING_BUNDLE_KEY) && args.containsKey(MESSAGE_STRING_BUNDLE_KEY)) {
                String title = args.getString(TITLE_STRING_BUNDLE_KEY);
                String message = args.getString(MESSAGE_STRING_BUNDLE_KEY);
                builder.setTitle(title);
                builder.setMessage(message);
            }
            int positiveBtnId;
            if (args.containsKey(POSITIVE_BTN_INT_BUNDLE_KEY)) {
                positiveBtnId = args.getInt(POSITIVE_BTN_INT_BUNDLE_KEY);
            } else {
                positiveBtnId = android.R.string.ok;
            }
            builder.setPositiveButton(positiveBtnId, clickListener);

            int negativeBtnId = 0;
            if (args.containsKey(NEGATIVE_BTN_INT_BUNDLE_KEY)) {
                negativeBtnId = args.getInt(NEGATIVE_BTN_INT_BUNDLE_KEY);
                builder.setNegativeButton(negativeBtnId, negativeClickListener);
            } else if (negativeClickListener != null) {
                builder.setNegativeButton(android.R.string.cancel, negativeClickListener);
            }

            final AlertDialog alertDialog = builder.create();

            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
                    if (btnPositive != null) {
                        btnPositive.setAllCaps(false);
                    }

                    Button btnNegative = alertDialog.getButton(Dialog.BUTTON_NEGATIVE);
                    if (btnNegative != null) {
                        btnNegative.setAllCaps(false);
                    }
                }
            });

            return alertDialog;
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            super.onCancel(dialog);
            if (cancelListener != null)
                cancelListener.onCancel(dialog);
        }

        public void setPositiveOnClickListener(DialogInterface.OnClickListener listener) {
            clickListener = listener;
        }

        public void setNegativeOnClickListener(DialogInterface.OnClickListener listener) {
            negativeClickListener = listener;
        }

        public void setOnCancelListener(DialogInterface.OnCancelListener listener) {
            cancelListener = listener;
        }
    }
}
