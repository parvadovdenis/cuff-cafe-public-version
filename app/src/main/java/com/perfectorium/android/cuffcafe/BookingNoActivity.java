package com.perfectorium.android.cuffcafe;

import android.view.View;
import android.widget.ImageView;

import com.perfectorium.android.cuffcafe.config.PrefManager;

/**
 * Created by Denys Parvadov on 03.11.16.
 */

public class BookingNoActivity extends BaseActivity {
    @Override
    protected int getContentView() {
        return R.layout.activity_booking_no;
    }

    @Override
    protected Class getCurrentClass() {
        return BookingActivity.class;
    }

    @Override
    protected void initActivityViews() {
        super.initActivityViews();
        ImageView ok = (ImageView) findViewById(R.id.booking_no_button_ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity();
            }
        });
    }

    protected void startActivity() {
        PrefManager prefManager = new PrefManager(this);
        prefManager.removeBookingId(true);
        super.startActivity(BookingActivity.class);

    }
}
