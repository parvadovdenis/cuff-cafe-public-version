package com.perfectorium.android.cuffcafe;

import android.view.View;
import android.widget.ImageView;

/**
 * Created by Denys Parvadov on 03.11.16.
 */

public class BookingYesActivity extends BaseActivity {
    @Override
    protected int getContentView() {
        return R.layout.activity_booking_yes;
    }

    @Override
    protected Class getCurrentClass() {
        return BookingActivity.class;
    }

    @Override
    protected void attachActivityViews() {
        super.attachActivityViews();
        ImageView cheers = (ImageView) findViewById(R.id.cheers);
        cheers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(BookingYesCheckActivity.class);
            }
        });
    }

    @Override
    protected void startActivity(Class activityClass) {
        super.startActivity(activityClass);
    }
}
