package com.perfectorium.android.cuffcafe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.aigestudio.wheelpicker.WheelPicker;

/**
 * Created by Denys Parvadov on 03.11.16.
 */

public class PeopleCountActivity extends Activity implements WheelPicker.OnItemSelectedListener {
    private WheelPicker wheel;
    Object data;

    private static String name;
    private static String phone;
    private static String email;

    public static void saveEditText(String n, String ph, String em){
        name = n;
        phone = ph;
        email = em;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.people);
        super.onCreate(savedInstanceState);
        wheel = (WheelPicker) findViewById(R.id.people_wheel);
        wheel.setOnItemSelectedListener(this);
        TextView ok = (TextView) findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data == null)
                    data = 1;
                BookingActivity.setPeopleCount(Integer.parseInt(String.valueOf(data)));
                startActivity(BookingActivity.class);
            }
        });
    }

    @Override
    public void onItemSelected(WheelPicker picker, Object data, int position) {
        this.data = data;
    }

    private void startActivity(Class activityClass) {
        BookingActivity.saveEditText(name, phone, email);
        Intent i = new Intent(this, activityClass);
        startActivity(i);
    }
}
