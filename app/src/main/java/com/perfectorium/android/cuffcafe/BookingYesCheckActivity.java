package com.perfectorium.android.cuffcafe;

import android.view.View;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.onesignal.OneSignal;
import com.perfectorium.android.cuffcafe.config.FireBaseConfig;
import com.perfectorium.android.cuffcafe.config.PrefManager;
import com.perfectorium.android.cuffcafe.config.TextFont;
import com.perfectorium.android.cuffcafe.models.Bookings;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * Created by Denys Parvadov on 08.11.16.
 */

public class BookingYesCheckActivity extends BaseActivity {
    private String booking_id;
    private String name;
    private int count_people;
    private String date;
    private PrefManager prefManager;

    private TextView n;
    private TextView d;
    private TextView hGd;
    private TextView cP;


    @Override
    protected int getContentView() {
        return R.layout.activity_booking_yes_check;
    }

    @Override
    protected Class getCurrentClass() {
        return BookingActivity.class;
    }

    @Override
    protected void attachActivityViews() {
        super.attachActivityViews();

        n = (TextView) findViewById(R.id.check_name);
        d = (TextView) findViewById(R.id.check_date);
        cP = (TextView) findViewById(R.id.check_people);
        hGd = (TextView) findViewById(R.id.have_a_great_day);

        TextFont.setJosefinsSansRegular(n, this);
        TextFont.setJosefinsSansRegular(d, this);
        TextFont.setJosefinsSansRegular(cP, this);
        TextFont.setJosefinsSansRegular(hGd, this);

        prefManager = new PrefManager(this);

        TextView cancel_booking = (TextView) findViewById(R.id.cancel_booking);
        cancel_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeBooking();
                startActivity(BookingActivity.class);
            }
        });
        TextView done = (TextView) findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prefManager.removeBookingId(true);
                startActivity(BookingActivity.class);
            }
        });
    }

    private void removeBooking() {
        final DatabaseReference scoresRef = FirebaseDatabase.getInstance().getReference("bookings");
        DatabaseReference newRef = scoresRef.child(booking_id);
        newRef.child("status").setValue(3);

        Firebase.setAndroidContext(this);
        Firebase ref = new Firebase(FireBaseConfig.FIREBASE_URL);
        ref.addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot postSnapShot : snapshot.child("admins").getChildren()) {
                    String token = String.valueOf(postSnapShot.child("adminToken").getValue());
                    String mas = name + " cancelled a booking request";
                    try {
                        OneSignal.postNotification(new JSONObject("{'contents': {'en':'" + mas + "'}, 'include_player_ids': ['" + token + "']}"), null);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
        prefManager.removeBookingId(true);
    }

    @Override
    protected void setFireBase() {
        PrefManager prefManager = new PrefManager(this);
        booking_id = prefManager.getBoookingId();
        Firebase.setAndroidContext(this);
        Firebase ref = new Firebase(FireBaseConfig.FIREBASE_URL);
        DatabaseReference scoresRef = FirebaseDatabase.getInstance().getReference("bookings");
        scoresRef.keepSynced(true);
        ref.addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot postSnapShot : snapshot.child("bookings").getChildren()) {
                    Bookings booking = postSnapShot.getValue(Bookings.class);
                    String getKey = postSnapShot.getKey();
                    if (getKey.equals(booking_id)) {
                        count_people = booking.getPeople();
                        date = String.valueOf(booking.getTime_booked());
                        name = booking.getName();
                        long dv = Long.valueOf(date) * 1000;// its need to be in milisecond
                        Date df = new java.util.Date(dv);
                        StringBuffer str = new StringBuffer();
                        str.append(new SimpleDateFormat("EEEE, dd MMMM yyyy").format(df));
                        str.append(" in ");
                        str.append(new SimpleDateFormat("hh:mm a").format(dv));
                        date = String.valueOf(str);
                        if (date.contains("12:00 PM"))
                            date = date.replace("PM", "AM");
                        if (date.contains("12:30 PM"))
                            date = date.replace("PM", "AM");
                    }
                }
                n.setText(name + ", we expect you in our restaurant");
                d.setText(date);
                cP.setText(String.valueOf("Table for " + count_people + " people"));
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }
}
