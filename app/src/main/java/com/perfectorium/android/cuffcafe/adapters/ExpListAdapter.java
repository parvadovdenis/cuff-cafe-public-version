package com.perfectorium.android.cuffcafe.adapters;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;

import com.perfectorium.android.cuffcafe.R;
import com.perfectorium.android.cuffcafe.config.MenuText;
import com.perfectorium.android.cuffcafe.models.Menus;

import java.util.ArrayList;

/**
 * Created by Denys Parvadov on 16.11.16.
 */

public class ExpListAdapter extends BaseExpandableListAdapter {

    private ArrayList<Menus> menus;
    private Context context;

    public ExpListAdapter(Context context, ArrayList<Menus> menus) {
        this.context = context;
        this.menus = menus;
    }

    @Override
    public int getGroupCount() {
        return menus.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return menus.get(groupPosition).getDishes().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return menus.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return menus.get(groupPosition).getDishes().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.menu_group, null);
        }
        ImageView arrow = (ImageView) convertView.findViewById(R.id.menu_group_arrow);
        if (isExpanded) {
            arrow.setImageResource(R.drawable.arrow_selected);
            //Изменяем что-нибудь, если текущая Group раскрыта
        } else {
            arrow.setImageResource(R.drawable.arrow_disabled);
            //Изменяем что-нибудь, если текущая Group скрыта
        }

        MenuText textGroup = (MenuText) convertView.findViewById(R.id.menu_group);
        textGroup.setText(menus.get(groupPosition).getTitle().toUpperCase());

        return convertView;

    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.menu_child, null);
        }

        String title = menus.get(groupPosition).getDishes().get(childPosition).getTitle().toUpperCase() +
                " - " + menus.get(groupPosition).getDishes().get(childPosition).getDescription();
        String price = menus.get(groupPosition).getDishes().get(childPosition).getPrice();

        MenuText mChTitle = (MenuText) convertView.findViewById(R.id.menu_child_title);
        MenuText mChPrice = (MenuText) convertView.findViewById(R.id.menu_child_price);

        SpannableString text = new SpannableString(title);
        String[] arr = title.split(" - ");
        char[] cArr = arr[0].toCharArray();
        text.setSpan(new RelativeSizeSpan(1), 0, cArr.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new RelativeSizeSpan(0.85f), cArr.length, title.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        mChTitle.setText(text);
        mChPrice.setText(price);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}