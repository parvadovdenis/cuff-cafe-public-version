package com.perfectorium.android.cuffcafe.models;

/**
 * Created by Denys Parvadov on 24.10.16.
 */

public class Dishes {

    private String price;
    private String title;
    private String id;
    private String description;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Dishes() {

    }
}
