package com.perfectorium.android.cuffcafe.config;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Denys Parvadov on 28.10.16.
 */

public class TextFont {
    private ArrayList<TextView> list;

    public static void setJosefinsSansRegular(TextView textView, Context c) {
        Typeface font = Typeface.createFromAsset(c.getAssets(), "JosefinSans-Regular.ttf");
        textView.setTypeface(font);
    }

    public static void setJosefinsSansSemiBoldItalic(TextView textView, Context c) {
        Typeface font = Typeface.createFromAsset(c.getAssets(), "JosefinSans-SemiBoldItalic.ttf");
        textView.setTypeface(font);
    }

    public static void setRobotoRegular(TextView textView, Context c) {
        Typeface font = Typeface.createFromAsset(c.getAssets(), "Roboto-Regular.ttf");
        textView.setTypeface(font);
    }
}
