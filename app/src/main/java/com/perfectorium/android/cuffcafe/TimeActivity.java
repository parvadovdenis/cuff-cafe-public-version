package com.perfectorium.android.cuffcafe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.aigestudio.wheelpicker.WheelPicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by Denys Parvadov on 03.11.16.
 */

public class TimeActivity extends Activity implements WheelPicker.OnItemSelectedListener {

    private WheelPicker wheel;
    private Object data;

    private static String name;
    private static String phone;
    private static String email;

    public static void saveEditText(String n, String ph, String em) {
        name = n;
        phone = ph;
        email = em;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.people);
        super.onCreate(savedInstanceState);
        ImageView header = (ImageView) findViewById(R.id.people_header);
        header.setImageResource(R.drawable.time_header);
        wheel = (WheelPicker) findViewById(R.id.people_wheel);
        wheel.setData(setList());
        wheel.setOnItemSelectedListener(this);
        TextView ok = (TextView) findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data == null)
                    data = "08 : 00 AM";
                BookingActivity.setTimeBooked(String.valueOf(data));
                startActivity(BookingActivity.class);
            }
        });
    }

    private List setList() {


        List<String> list = new ArrayList<>();
        DateFormat dateFormat = new SimpleDateFormat("hh : mm a");
        for (int i = 8; i < 22; i++) {
            for (int j = 0; j < 2; j++) {
                Date d = new Date();
                d.setHours(i);
                if (j == 0)
                    d.setMinutes(0);
                else if (j == 1)
                    d.setMinutes(30);
                if (dateFormat.format(d).equals("12 : 00 PM"))
                    list.add("12 : 00 AM");
                else if (dateFormat.format(d).equals("12 : 30 PM"))
                    list.add("12 : 30 AM");
                else
                    list.add(dateFormat.format(d));
            }

        }
        return list;
    }

    @Override
    public void onItemSelected(WheelPicker picker, Object data, int position) {
        this.data = data;
    }

    private void startActivity(Class activityClass) {
        BookingActivity.saveEditText(name, phone, email);

        Intent i = new Intent(this, activityClass);
        startActivity(i);
    }
}
