package com.perfectorium.android.cuffcafe.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.perfectorium.android.cuffcafe.MainActivity;
import com.perfectorium.android.cuffcafe.R;
import com.perfectorium.android.cuffcafe.config.PrefManager;

/**
 * Created by Denys Parvadov on 16.10.16.
 */

public class SwipeAdapter extends PagerAdapter {
    private int[] image_list;
    private Context context;
    private LayoutInflater layoutInflater;

    private boolean ifContact;
    private PrefManager prefManager;

    private int position;

    public SwipeAdapter(Context context, PrefManager prefManager, boolean bool) {
        this.prefManager = prefManager;
        this.ifContact = bool;
        this.context = context;
    }

    public int[] setList(int[] slider_content) {
        image_list = slider_content;
        return image_list;
    }

    @Override
    public int getCount() {
        return image_list.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    public int getPosition() {
        return this.position;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item = layoutInflater.inflate(R.layout.view_pager_content, container, false);
        ImageView imageView = (ImageView) item.findViewById(R.id.view_pager_content);
        imageView.setImageResource(image_list[position]);
        if (!ifContact) {
            TextView welcome = (TextView) item.findViewById(R.id.welcome);
            TextView text = (TextView) item.findViewById(R.id.intro_1_text);
            ImageView label = (ImageView) item.findViewById(R.id.intro_label);
            label.setImageResource(R.drawable.intro_label);
            ImageView begin_button = (ImageView) item.findViewById(R.id.begin_button);
            int full_begin_button = R.drawable.begin_button;
            int empty_begin_button = 0;
            switch (position) {
                case 0:
                    begin_button.setImageResource(empty_begin_button);
                    label.getLayoutParams().height = 230;
                    label.requestLayout();
                    welcome.setText(R.string.welcome);
                    text.setText(R.string.intro_1_text);
                    break;
                case 1:
                    begin_button.setImageResource(empty_begin_button);
                    label.getLayoutParams().height = 0;
                    label.requestLayout();
                    welcome.setText(R.string.easy_booking);
                    text.setText(R.string.intro_2_text);
                    break;
                case 2:
                    begin_button.setImageResource(full_begin_button);
                    begin_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            prefManager.setFirstTimeLaunch(false);
                            context.startActivity(new Intent(context, MainActivity.class));
                        }
                    });
                    label.getLayoutParams().height = 0;
                    label.requestLayout();
                    welcome.setText(R.string.special_offers);
                    text.setText(R.string.intro_3_text);
                    break;
            }
            Typeface font = Typeface.createFromAsset(context.getAssets(), "JosefinSans-Regular.ttf");
            text.setTypeface(font);
        }

        this.position = position;
        container.addView(item);

        return item;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
