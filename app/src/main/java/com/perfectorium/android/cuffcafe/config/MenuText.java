package com.perfectorium.android.cuffcafe.config;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Denys Parvadov on 16.11.16.
 */

public class MenuText extends TextView {
    public MenuText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "JosefinSans-Regular.ttf"));
    }
}
