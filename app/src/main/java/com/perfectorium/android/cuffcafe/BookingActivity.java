package com.perfectorium.android.cuffcafe;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.onesignal.OneSignal;
import com.perfectorium.android.cuffcafe.config.FireBaseConfig;
import com.perfectorium.android.cuffcafe.config.PrefManager;
import com.perfectorium.android.cuffcafe.models.Bookings;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * Created by Denys Parvadov on 16.10.16.
 */

public class BookingActivity extends BaseActivity {

    private PrefManager prefManager;

    private DatabaseReference mDataBase;
    private String booking_id;

    private EditText edit_name;
    private EditText edit_phone;
    private EditText edit_email;

    public int layout = R.layout.activity_booking;

    private static Bookings booking = new Bookings();
    private static String time = "08 : 00 AM";
    private static Date booking_date = new Date();
    private static int month;
    private static int year;

    private static String name;
    private static String phone;
    private static String email;


    private Date test;

    private long book_date;

    private long setBookDate(Date date, String t) {
        String[] arr = t.split(" ");
        int h = Integer.parseInt(arr[0]);
        int m = Integer.parseInt(arr[2]);
        String apm = arr[3];
        if (apm.equals("PM")) {
            h = h + 12;
        }
        date.setHours(h);
        date.setMinutes(m);
        test = date;
        return date.getTime() / 1000;
    }

    private long getBookedMadeTime() {
        return System.currentTimeMillis() / 1000L;
    }

    private void loadFormText() {
        edit_name = (EditText) findViewById(R.id.edit_name);
        edit_phone = (EditText) findViewById(R.id.edit_phone);
        edit_email = (EditText) findViewById(R.id.edit_email);

        edit_name.setText(name);
        edit_phone.setText(phone);
        edit_email.setText(email);
    }

    private void setFromEditText() {
        edit_name = (EditText) findViewById(R.id.edit_name);
        edit_phone = (EditText) findViewById(R.id.edit_phone);
        edit_email = (EditText) findViewById(R.id.edit_email);

        this.name = String.valueOf(edit_name.getText());
        this.phone = String.valueOf(edit_phone.getText());
        this.email = String.valueOf(edit_email.getText());
    }

    public static void setTimeBooked(String time_booked) {
        time = time_booked;
    }

    private String getTime() {
        return this.time;
    }

    public static void setPeopleCount(int people_count) {
        booking.setPeople(people_count);
    }

    public static void setBookedDate(Date d) {
        booking_date = d;
    }

    private void setLayout(int layout) {
        this.layout = layout;
    }

    @Override
    protected int getContentView() {
        return layout;
    }

    @Override
    protected Class getCurrentClass() {
        return BookingActivity.class;
    }

    @Override
    protected void setFireBase() {
        PrefManager prefManager = new PrefManager(this);
        final String booking_id = prefManager.getBoookingId();

        Firebase.setAndroidContext(this);
        Firebase ref = new Firebase(FireBaseConfig.FIREBASE_URL);
        ref.addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot postSnapShot : snapshot.child("bookings").getChildren()) {
                    Bookings booking = postSnapShot.getValue(Bookings.class);
                    String getKey = postSnapShot.getKey();
                    if (getKey.equals(booking_id))
                        switch (booking.getStatus()) {
                            case 0:
                                setLayout(R.layout.activity_booking_wait);
                                break;
                            case 1:
                                setLayout(R.layout.activity_booking_yes);
                                break;
                            case 2:
                                setLayout(R.layout.activity_booking_no);
                                break;
                        }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }

    public static void saveEditText(String n, String ph, String em) {
        name = n;
        phone = ph;
        email = em;
    }

    private void makeToast(String message) {
        Toast.makeText(getApplicationContext(), message,
                Toast.LENGTH_LONG).show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void bookingViews() {
        prefManager = new PrefManager(this);
        if (prefManager.getBoookingId() != null)
            this.booking_id = prefManager.getBoookingId();

        loadFormText();
        setFromEditText();
        RelativeLayout date = (RelativeLayout) findViewById(R.id.date);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CalendarActivity.checkedDate = booking_date;
                startActivity(CalendarActivity.class);
            }
        });
        final RelativeLayout time_wheel = (RelativeLayout) findViewById(R.id.time_wheel);
        time_wheel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(TimeActivity.class);
            }
        });

        RelativeLayout people = (RelativeLayout) findViewById(R.id.people_wheel);
        people.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(PeopleCountActivity.class);
            }
        });

        ImageView make_a_booking = (ImageView) findViewById(R.id.make_a_booking);
        make_a_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setOneSignalToken();

                String name = String.valueOf(edit_name.getText());
                String email = String.valueOf(edit_email.getText());
                String phone = String.valueOf(edit_phone.getText());
                long time_booked = 0;
                long time_made = getBookedMadeTime();
                if (booking_date == null)
                    booking_date = new Date();
                if (!Objects.equals(time, "HH:MM") && booking_date != null)
                    time_booked = setBookDate(booking_date, time);

                if (name == null || Objects.equals(name, "") || email == null || Objects.equals(email, "")
                        || phone == null || Objects.equals(phone, "") || time_booked == 0)
                    makeToast("Invalid input");
                else {
                    makeBooking(name, email, phone, time_made, time_booked);
                    startActivity(BookingWaitActivity.class);
                }
            }
        });

        TextView time_text = (TextView) findViewById(R.id.time_text);
        time_text.setText(setFormTime(this.getTime()));
        TextView people_count_text = (TextView) findViewById(R.id.people_count_text);
        people_count_text.setText(String.valueOf(booking.getPeople()));
        TextView date_text = (TextView) findViewById(R.id.date_text);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        if (booking_date != null)
            date_text.setText(formatter.format(booking_date));
        else
            date_text.setText((formatter.format(new Date())));
    }

    private String setFormTime(String time) {
//        DateFormat dateFormat = new SimpleDateFormat("hh : mm a");
//        Date d = new Date();
//        String[] arr = time.split(" ");
//        int h = Integer.parseInt(arr[0]);
//        int m = Integer.parseInt(arr[2]);
//        String apm = arr[3];
//        if (apm.equals("PM")) {
//            h = h + 12;
//        }
//        d.setHours(h);
//        if (m == 0)
//            d.setMinutes(0);
//        if (m == 30)
//            d.setMinutes(30);
//        BookingActivity.time = dateFormat.format(d);
        BookingActivity.time = time;
        return BookingActivity.time;
    }

    private void setOneSignalToken() {
    }

    private void makeBooking(String name, String email, String phone, long time_made, long time_booked) {
        DatabaseReference scoresRef = FirebaseDatabase.getInstance().getReference("bookings");

        booking.setEmail(String.valueOf(edit_email.getText()));
        booking.setName(String.valueOf(edit_name.getText()));
        booking.setPhone(String.valueOf(edit_phone.getText()));
        booking.setStatus(0);
        booking.setTime_made(getBookedMadeTime());
        booking.setTime_booked(setBookDate(booking_date, time));
        OneSignal.startInit(this).init();
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                Log.d("debug", "User:" + userId);
                if (registrationId != null)
                    Log.d("debug", "registrationId:" + registrationId);
                booking.setToken(userId);
            }
        });
        DatabaseReference newRef = scoresRef.push();
        booking.setId(newRef.getKey());
        newRef.setValue(booking);
        prefManager.setBookingId(newRef.getKey());


        Firebase.setAndroidContext(this);
        Firebase ref = new Firebase(FireBaseConfig.FIREBASE_URL);
        ref.addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot postSnapShot : snapshot.child("admins").getChildren()) {
                    String token = String.valueOf(postSnapShot.child("adminToken").getValue());
                    String mas = "New booking request from " + booking.getName();
                    try {
                        OneSignal.postNotification(new JSONObject("{'contents': {'en':'" + mas + "'}, 'include_player_ids': ['" + token + "']}"), null);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }

    @Override
    protected void startActivity(Class activityClass) {
        String name;
        String phone;
        String email;

        name = String.valueOf(edit_name.getText());
        phone = String.valueOf(edit_phone.getText());
        email = String.valueOf(edit_email.getText());

        if (activityClass == CalendarActivity.class)
            CalendarActivity.saveEditText(name, phone, email);
        else if (activityClass == PeopleCountActivity.class)
            PeopleCountActivity.saveEditText(name, phone, email);
        else if (activityClass == TimeActivity.class)
            TimeActivity.saveEditText(name, phone, email);

        super.startActivity(activityClass);
    }

}
