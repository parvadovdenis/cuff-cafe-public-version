package com.perfectorium.android.cuffcafe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.perfectorium.android.cuffcafe.config.CalendarView;

import java.util.Date;

/**
 * Created by Denys Parvadov on 02.11.16.
 */

public class CalendarActivity extends Activity {

    private static Date date;
    public static Date checkedDate;
    private static String name;
    private static String phone;
    private static String email;

    public static void saveEditText(String n, String ph, String em) {
        name = n;
        phone = ph;
        email = em;
    }

    private void makeToast(String message) {
        Toast.makeText(getApplicationContext(), message,
                Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.calendar);
        super.onCreate(savedInstanceState);

        CalendarView cv = ((CalendarView) findViewById(R.id.calendar_view));
        if (checkedDate != null) {
            CalendarView.checkedDate = checkedDate;
            cv.setCurrentCalendar(checkedDate.getMonth());
            cv.updateCalendar();
        }
        // assign event handler
        cv.setEventHandler(new CalendarView.EventHandler() {
            @Override
            public void onDayLongPress(Date date) {
                // show returned day
                setDate(date);
            }
        });

        TextView ok = (TextView) findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (date == null)
                    date = new Date();
                else {
                    BookingActivity.setBookedDate(date);
                    startActivity(BookingActivity.class);
                }
            }
        });
    }

    public static void setDate(Date d) {
        date = d;
    }

    private void startActivity(Class activityClass) {
        BookingActivity.saveEditText(name, phone, email);
        Intent i = new Intent(this, activityClass);
        startActivity(i);
    }
}
