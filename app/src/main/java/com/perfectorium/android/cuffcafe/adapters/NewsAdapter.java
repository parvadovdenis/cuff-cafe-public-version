package com.perfectorium.android.cuffcafe.adapters;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.perfectorium.android.cuffcafe.R;
import com.perfectorium.android.cuffcafe.config.TextFont;
import com.perfectorium.android.cuffcafe.models.News;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Denys Parvadov on 19.10.16.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {
    private OnClickListener listener;
    private List<News> news;
    protected static Context context;

    public NewsAdapter(ArrayList<News> news, OnClickListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        this.news = news;
    }


    @Override
    public NewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_list_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.bind(news.get(position));
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(news.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    public interface OnClickListener {
        void onClick(News news);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView itemName;
        public ImageView itemImage;
        public ImageView itemLine;
        public LinearLayout item;

        public ViewHolder(View v) {
            super(v);
            item = (LinearLayout) v.findViewById(R.id.item_linear);
            itemName = (TextView) v.findViewById(R.id.news_item_text);
            itemImage = (ImageView) v.findViewById(R.id.news_item_image);
            itemLine = (ImageView) v.findViewById(R.id.news_item_line);
        }

        public void bind(final News model) {
            this.itemName.setText(model.getTitle());
            TextFont.setJosefinsSansRegular(this.itemName, context);
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            final int width = size.x;
            final int height = size.y;
            this.itemImage.getLayoutParams().width = width;
            this.itemImage.getLayoutParams().height = height / 3;
            this.itemImage.requestLayout();
            Picasso.with(context).load(model.getImage()).placeholder(R.drawable.not).
                    networkPolicy(NetworkPolicy.OFFLINE)
//                    .resize(width, height / 3)
                    .into(this.itemImage, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    //Try again online if cache failed
                    Picasso.with(context)
                            .load(model.getImage())
//                            .resize(width, height / 3)
                            .into(itemImage);
                }
            });

            this.itemLine.setImageResource(R.drawable.home_news_line);
        }
    }

}
