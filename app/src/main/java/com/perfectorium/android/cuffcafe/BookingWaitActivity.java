package com.perfectorium.android.cuffcafe;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.perfectorium.android.cuffcafe.config.FireBaseConfig;
import com.perfectorium.android.cuffcafe.config.PrefManager;
import com.perfectorium.android.cuffcafe.models.Bookings;

import java.util.Objects;

/**
 * Created by Denys Parvadov on 03.11.16.
 */

public class BookingWaitActivity extends BaseActivity {
    @Override
    protected int getContentView() {
        return R.layout.activity_booking_wait;
    }

    @Override
    protected Class getCurrentClass() {
        return BookingActivity.class;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImageView wait = (ImageView) findViewById(R.id.wait_image);
        Animation rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        wait.startAnimation(rotate);
    }

    @Override
    protected void setFireBase() {

        final PrefManager prefManager = new PrefManager(this);
        Firebase.setAndroidContext(this);
        final Firebase ref = new Firebase(FireBaseConfig.FIREBASE_URL);
        ref.addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                String booking_id = prefManager.getBoookingId();
                for (DataSnapshot postSnapShot : snapshot.child("bookings").getChildren()) {
                    Bookings booking = postSnapShot.getValue(Bookings.class);
                    String getKey = postSnapShot.getKey();
                    Class cl = null;
                    if (getKey.equals(booking_id))
                        switch (booking.getStatus()) {
                            case 1:
                                cl = BookingYesActivity.class;
                                break;
                            case 2:
                                cl = BookingNoActivity.class;
                                break;
                        }
                    if (cl != null)
                        startActivity(cl);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });

    }
}
