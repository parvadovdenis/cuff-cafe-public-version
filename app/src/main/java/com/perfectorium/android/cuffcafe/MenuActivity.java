package com.perfectorium.android.cuffcafe;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.perfectorium.android.cuffcafe.adapters.ExpListAdapter;
import com.perfectorium.android.cuffcafe.config.FireBaseConfig;
import com.perfectorium.android.cuffcafe.models.Dishes;
import com.perfectorium.android.cuffcafe.models.Menus;

import java.util.ArrayList;

/**
 * Created by Denys Parvadov on 16.10.16.
 */

public class MenuActivity extends BaseActivity {

    private ArrayList<Menus> list = new ArrayList<>();

//    private SimpleExpandableListAdapter adapter;
    private ExpListAdapter expListAdapter;

//    private Map<String, String> map;

//    private ArrayList<Map<String, String>> groupDataList = new ArrayList<>();

//    private ArrayList<ArrayList<Map<String, String>>> сhildDataList = new ArrayList<>();
//    private ArrayList<Map<String, String>> сhildDataItemList = new ArrayList<>();

    private ImageView arrow;

    @Override
    protected Class getCurrentClass() {
        return MenuActivity.class;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_menu;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void setMenu() {
        ExpandableListView expandableListView = (ExpandableListView) findViewById(R.id.menu);
        expListAdapter = new ExpListAdapter(this, list);
        expandableListView.setAdapter(expListAdapter);
    }

    @Override
    protected void setFireBase() {
        Firebase.setAndroidContext(this);
        Firebase ref = new Firebase(FireBaseConfig.FIREBASE_URL);
        DatabaseReference scoresRef = FirebaseDatabase.getInstance().getReference("menus");
        scoresRef.keepSynced(true);
        ref.addValueEventListener(new com.firebase.client.ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot snapshot) {
                list.clear();
                for (DataSnapshot postSnapShot : snapshot.child("menus").getChildren()) {
                    Menus menu = postSnapShot.getValue(Menus.class);
                    ArrayList<Dishes> dishes = new ArrayList<>();

                    for (DataSnapshot dt : snapshot.child("menus").
                            child(postSnapShot.getKey()).child("dishes").getChildren()) {
                        Dishes d = dt.getValue(Dishes.class);
                        dishes.add(d);
                    }
                    menu.setDishes(dishes);
                    list.add(menu);
                }
                expListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }
}
