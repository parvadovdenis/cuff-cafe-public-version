package com.perfectorium.android.cuffcafe.config;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Denys Parvadov on 26.10.16.
 */

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    String booking_id;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "androidhive-welcome";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String BOOKING_ID = "BOOKING_ID";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setBookingId(String booking_id) {
//        this.booking_id = booking_id;
        editor.putString(BOOKING_ID, booking_id);
        editor.commit();
    }

    public String getBoookingId() {
        return pref.getString(BOOKING_ID, booking_id);
    }

    public void removeBookingId(boolean bool) {
        if (bool)
            editor.putString(BOOKING_ID, "");
        editor.commit();
    }

}