package com.perfectorium.android.cuffcafe;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.perfectorium.android.cuffcafe.adapters.SwipeAdapter;
import com.perfectorium.android.cuffcafe.config.PrefManager;

import java.util.ArrayList;

/**
 * Created by Denys Parvadov on 17.10.16.
 */

public class IntroActivity extends AppCompatActivity {

    private PrefManager prefManager;
    private ViewPager viewPager;
    private SwipeAdapter swipeAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        prefManager = new PrefManager(this);
        if (!prefManager.isFirstTimeLaunch()) {
            launchHomeScreen();
            finish();
        }
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_intro);

        ImageView in1 = (ImageView) findViewById(R.id.intro_indicator1);
        ImageView in2 = (ImageView) findViewById(R.id.intro_indicator2);
        ImageView in3 = (ImageView) findViewById(R.id.intro_indicator3);

        changePageClickDots(in1, 0);
        changePageClickDots(in2, 1);
        changePageClickDots(in3, 2);

        viewPager = (ViewPager) findViewById(R.id.intro_viewPager);
        swipeAdapter = new SwipeAdapter(this, prefManager, false);
        swipeAdapter.setList(setListContent());
        viewPager.setAdapter(swipeAdapter);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                setIntroView(position);
            }

            @Override
            public void onPageSelected(final int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void changePageClickDots(ImageView im, final int position) {
        im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(position);
            }
        });
    }

    private void setIntroView(int position) {
        View v = findViewById(R.id.intro_indicator_dots);
        ImageView in1 = (ImageView) v.findViewById(R.id.intro_indicator1);
        ImageView in2 = (ImageView) v.findViewById(R.id.intro_indicator2);
        ImageView in3 = (ImageView) v.findViewById(R.id.intro_indicator3);

        ArrayList<ImageView> dots = new ArrayList<>();

        dots.add(in1);
        dots.add(in2);
        dots.add(in3);

        int selected = 20;
        int unselected = 15;

        for (ImageView dot : dots) {
            dot.getLayoutParams().height = unselected;
            dots.get(position).getLayoutParams().height = selected;
        }
    }

    private void launchHomeScreen() {
        prefManager.setFirstTimeLaunch(false);
        startActivity(new Intent(IntroActivity.this, MainActivity.class));
        finish();
    }

    private int[] setListContent() {
        return new int[]{
                R.drawable.intro1_background,
                R.drawable.intro2_background,
                R.drawable.intro3_background
        };
    }
}
